using System.Collections;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }

    [SerializeField] TMP_InputField _userNameFieldMain, _passwordFieldMain, _userOldNameFieldChange, _userNewNameFieldChange, _passwordFieldChange;

    [Space]
    [SerializeField] GameObject _mainMenuLayout, _rankingLayout, _gameplayLayout, _changeUserNameLayout;
    [SerializeField] GameObject _decorationTerrain;

    [Space]
    [SerializeField] TextMeshProUGUI _userNameText;
    [SerializeField] TextMeshProUGUI _scoreText;
    
    [Space]
    [SerializeField] Image _buttonBoostMask;
    [SerializeField] Animator _playerModelAnim ,_boostButtonAnim, _mainButtonAnim, _terrainAnim;

    private void OnEnable()
    {
        GameManager.Instance.Server.OnPlayerRecieved += OpenGameplayLayout;
        GameManager.Instance.Server.OnUserDataUpdated += GoToMainLayout;
        GameManager.Instance.Server.OnRankingRecieved += OpenRankingLayout;
    }

    private void OnDisable()
    {
        GameManager.Instance.Server.OnPlayerRecieved -= OpenGameplayLayout;
        GameManager.Instance.Server.OnUserDataUpdated -= GoToMainLayout;
        GameManager.Instance.Server.OnRankingRecieved -= OpenRankingLayout;
    }

    public void UpdateLayout()
    {
        UpdateUserName();
        UpdateScore();
    }

    internal void OnClickMainButton()
    {
        _mainButtonAnim.SetTrigger("OnClick");
        UpdateScore();
        UpdateUserName();
    }

    private void UpdateUserName()
    {
        if (GameManager.Instance._player.UserName != null)
            _userNameText.text = GameManager.Instance._player.UserName;
        else
            _userNameText.text = "!!!Error";
    }
    private void UpdateScore() => _scoreText.text = "Score: " + GameManager.Instance._player.Score.ToString();

    public IEnumerator OnBoost()
    {
        _boostButtonAnim.SetTrigger("ActiveState");

        yield return new WaitForSeconds(GameManager.Instance.MultiplierBoostDuration);

        _buttonBoostMask.fillAmount = 0;
        IncreaseFillAmount();
        _boostButtonAnim.SetTrigger("DisabledState");

        yield return new WaitForSeconds(GameManager.Instance.RechargeButtonTime);

        _boostButtonAnim.SetTrigger("DefaultState");
    }

    private async void IncreaseFillAmount()
    {
        float cooldown = 0;
        while (_buttonBoostMask.fillAmount < 1)
        {
            cooldown += Time.deltaTime;
            _buttonBoostMask.fillAmount = cooldown / GameManager.Instance.RechargeButtonTime;
            await Task.Yield();
        }
    }

    public void OpenGameplayLayout()
    {
        _mainMenuLayout.SetActive(false);

        _decorationTerrain.SetActive(true);
        _gameplayLayout.SetActive(true);
        UpdateLayout();
    }

    public void OpenRankingLayout()
    {
        _rankingLayout.SetActive(true);
        _mainMenuLayout.SetActive(false);
    }

    public void OpenChangeNameLayout()
    {
        _mainMenuLayout.SetActive(false);
        _changeUserNameLayout.SetActive(true);
    }


    public void GoToMainLayout()
    {
        _mainMenuLayout.SetActive(true);
       
        _rankingLayout.SetActive(false);
        _gameplayLayout.SetActive(false);
        _decorationTerrain.SetActive(false);
        _changeUserNameLayout.SetActive(false);
    }

    public void GoToGameplayLayoutBypass()
    {
        GameManager.Instance._player = new PlayerData();
        _gameplayLayout.SetActive(true);

     
        _mainMenuLayout.SetActive(false);
        
    }

    public void LogOut()
    {
        GameManager.Instance.Server.UpdateScoreOfAnUser(GameManager.Instance._player.UserName, GameManager.Instance._player.Password, GameManager.Instance._player.Score);
    }

    public void Ranking()
    {
        GameManager.Instance.Server.GetRanking();
    }

    public void OnDeleteUserButton()
    {
        GameManager.Instance.Server.DeleteUser(_userNameFieldMain.text, _passwordFieldMain.text);
        _userNameFieldMain.text = null;
        _passwordFieldMain.text = null;
    }

    public void OnLoginButton()
    {
        GameManager.Instance.Server.GetUser(_userNameFieldMain.text, _passwordFieldMain.text);
        _userNameFieldMain.text = null;
        _passwordFieldMain.text = null;
    }
    
    public void OnSignUpButton()
    {
        GameManager.Instance.Server.AddNewUser(_userNameFieldMain.text, _passwordFieldMain.text);
        GameManager.Instance.Server.GetUser(_userNameFieldMain.text, _passwordFieldMain.text);
        _userNameFieldMain.text = null;
        _passwordFieldMain.text = null;
    }

    public void OnChangeNameButton()
    {
        GameManager.Instance.Server.UpdateUserName(_userOldNameFieldChange.text, _userNewNameFieldChange.text, _passwordFieldChange.text);
        _userNewNameFieldChange.text = null;
        _userOldNameFieldChange.text = null;
        _passwordFieldChange.text = null;
    }

    private void Update()
    {
        _playerModelAnim.SetFloat("Walk", GameManager.Instance._player == null? 0 : Mathf.Min(GameManager.Instance._player.Score / 1000f, 1));
        _terrainAnim.speed = GameManager.Instance._player == null ? 0 : Mathf.Min(GameManager.Instance._player.Score / 1000f, 1);
    }

    public void CloseGame() => Application.Quit();
}
