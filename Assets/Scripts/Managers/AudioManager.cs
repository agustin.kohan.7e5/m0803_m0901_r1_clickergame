using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }

    [SerializeField] AudioSource _buttonAudioSource, _musicAudioSource;

    [Space]
    [SerializeField] AudioClip _gameplayMusic, _boostMusic;
    
    public void PlayAudio(AudioClip audio)
    {
        _buttonAudioSource.clip = audio;
        _buttonAudioSource.Play();
    }

    public void PlayMusic(string scene)
    {
        //Se pueden meter mas canciones por cada escena
        switch (scene)
        {
            case "Gameplay":
                _musicAudioSource.clip = _gameplayMusic;
                break;
            case "Boost":
                _musicAudioSource.clip = _boostMusic;
                break;
            default:
                break;
        }
        
        _musicAudioSource.Play();
    }
}
