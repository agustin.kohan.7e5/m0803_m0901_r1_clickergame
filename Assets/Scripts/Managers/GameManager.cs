using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }

    public Server Server;

    internal PlayerData _player;
    public PlayerData[] Ranking = new PlayerData[0];

    [Space]
    [SerializeField] int PointsXClick;
    public float _currentScore { get; private set; }
    public void AddPuntuation() 
    {
        _player.Score += PointsXClick * MultiplierBoost;
        UIManager.Instance.OnClickMainButton();
    }

    [Space]
    public Button BoostButton;
    public int BoostMultiplier;
    public int MultiplierBoostDuration;
    public int RechargeButtonTime;
    public int MultiplierBoost { get; private set; } = 1;

    public async void OnBoost()
    {
        AudioManager.Instance.PlayMusic("Boost");
        StartCoroutine(UIManager.Instance.OnBoost());
        
        BoostButton.interactable = false;
        MultiplierBoost *= BoostMultiplier;

        await Task.Delay(MultiplierBoostDuration * 1000);
        AudioManager.Instance.PlayMusic("Gameplay");
        MultiplierBoost /= BoostMultiplier;

        await Task.Delay(RechargeButtonTime * 1000);
        
        BoostButton.interactable = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.Instance.PlayMusic("Gameplay");
    }
}
