using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ranking : MonoBehaviour
{
    [SerializeField] GameObject _playerCardPrefab;
    [SerializeField] private Transform _contentTransform;

    List<PlayerCard> _playerCards = new List<PlayerCard>();

    private void OnEnable()
    {
        GenerateRanking(GameManager.Instance.Ranking);
    }

    public void GenerateRanking(PlayerData[] playerData)
    {
        for (int i = 0; i < playerData.Length; i++)
        {
            if (_playerCards.Count < playerData.Length)
                _playerCards.Add(Instantiate(_playerCardPrefab, _contentTransform).GetComponent<PlayerCard>());

            _playerCards[i].UpdateCardValues(playerData[i].UserName, playerData[i].Score);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_contentTransform.GetComponent<RectTransform>());
    }

}
