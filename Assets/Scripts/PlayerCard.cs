using UnityEngine;
using TMPro;

public class PlayerCard : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _playerNameText, _playerScoreText;
    
    public void UpdateCardValues(string username, int score)
    {
        _playerNameText.text = username;
        _playerScoreText.text = score.ToString();
    }
}
