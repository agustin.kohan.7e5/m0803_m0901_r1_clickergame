using Newtonsoft.Json;
using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class Server : MonoBehaviour
{
    public Action OnPlayerRecieved;
    public Action OnUserDataUpdated;
    public Action OnRankingRecieved;

    string _baseURL = "http://localhost:3023";


    void UpdateUserValues(PlayerData playerData) => GameManager.Instance._player = playerData;
    void UpdateRankingsData(PlayerData[] players) => GameManager.Instance.Ranking = players;

    public void AddNewUser(string newUser, string newPassword) => StartCoroutine(AddNewUserCoroutine(newUser, newPassword));
    public void GetUser(string username, string password) => StartCoroutine(GetUserCoroutine(username, password));
    public void UpdateUserName(string oldUserName, string newUserName, string password) => StartCoroutine(UpdateUserNameCoroutine(oldUserName, newUserName, password));
    public void UpdateScoreOfAnUser(string userName, string password, int score) => StartCoroutine(UpdateScoreOfAnUserCoroutine(userName, password, score));
    public void DeleteUser(string userName, string password) => StartCoroutine(DeleteUserCoroutine(userName, password));
    public void GetRanking() => StartCoroutine(GetRankingCoroutine());

    IEnumerator AddNewUserCoroutine(string newUser, string newPassword)
    {
        string path = _baseURL + "/addUser";
        using (UnityWebRequest serverConnection = new UnityWebRequest(path, "POST"))
        {
            string bodystring = "{\"username\":\"" + newUser + "\", \"password\":\"" + newPassword + "\"}";
            // convert JSON to raw bytes
            byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
            serverConnection.uploadHandler = new UploadHandlerRaw(rawBody);
            serverConnection.downloadHandler = new DownloadHandlerBuffer();
            serverConnection.SetRequestHeader("Content-Type", "application/json");

            yield return serverConnection.SendWebRequest();


            switch (serverConnection.result)
            {
                case UnityWebRequest.Result.InProgress:
                    break;
                case UnityWebRequest.Result.Success:
                    break;
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(serverConnection.error);
                    break;
                default:
                    break;
            }
            //Me pasa algo??
        }
    }

    IEnumerator GetUserCoroutine(string username, string password)
    {
        string path = _baseURL + "/getUser/" + username;
        using (UnityWebRequest serverConnection = new UnityWebRequest(path, "GET"))
        {
            string bodystring = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\"}";
            // convert JSON to raw bytes
            byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
            serverConnection.uploadHandler = new UploadHandlerRaw(rawBody);
            serverConnection.downloadHandler = new DownloadHandlerBuffer();
            serverConnection.SetRequestHeader("Content-Type", "application/json");

            yield return serverConnection.SendWebRequest();

            switch (serverConnection.result)
            {
                case UnityWebRequest.Result.InProgress:
                    break;
                case UnityWebRequest.Result.Success:
                    string jsonData = serverConnection.downloadHandler.text;
                    PlayerData UserData = JsonConvert.DeserializeObject<PlayerData>(jsonData);
                    UpdateUserValues(UserData);
                    GameManager.Instance._player.Password = password;
                    OnPlayerRecieved?.Invoke();
                    break;
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(serverConnection.error);
                    break;
                default:
                    break;
            }
        }
    }

    IEnumerator UpdateUserNameCoroutine(string oldUserName, string newUserName, string password)
    {
        string path = _baseURL + "/modifyUsername/" + oldUserName;
        using (UnityWebRequest serverConnection = new UnityWebRequest(path, "PUT"))
        {
            string bodystring = "{\"username\":\"" + newUserName + "\", \"password\":\"" + password + "\"}";
            // convert JSON to raw bytes
            byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
            serverConnection.uploadHandler = new UploadHandlerRaw(rawBody);
            serverConnection.downloadHandler = new DownloadHandlerBuffer();
            serverConnection.SetRequestHeader("Content-Type", "application/json");

            yield return serverConnection.SendWebRequest();

            switch (serverConnection.result)
            {
                case UnityWebRequest.Result.InProgress:
                    break;
                case UnityWebRequest.Result.Success:
                    OnUserDataUpdated?.Invoke();
                    break;
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(serverConnection.error);
                    break;
                default:
                    break;
            }
        }
    }

    private IEnumerator UpdateScoreOfAnUserCoroutine(string userName, string password,int newScore)
    {
        string path = _baseURL + "/update/" + userName + "/" + newScore;
        using (UnityWebRequest serverConnection = new UnityWebRequest(path, "PUT"))
        {
            string bodystring = "{\"username\":\"" + userName + "\", \"password\":\"" + password + "\"}";
            // convert JSON to raw bytes
            byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
            serverConnection.uploadHandler = new UploadHandlerRaw(rawBody);
            serverConnection.downloadHandler = new DownloadHandlerBuffer();
            serverConnection.SetRequestHeader("Content-Type", "application/json");

            yield return serverConnection.SendWebRequest();


            switch (serverConnection.result)
            {
                case UnityWebRequest.Result.InProgress:
                    break;
                case UnityWebRequest.Result.Success:
                    OnUserDataUpdated?.Invoke();
                    break;
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(serverConnection.error);
                    break;
                default:
                    break;
            }
        }
    }

    IEnumerator DeleteUserCoroutine(string userName, string password)
    {
        string path = _baseURL + "/delete/" + userName;
        using (UnityWebRequest serverConnection = new UnityWebRequest(path, "DELETE"))
        {
            string bodystring = "{\"username\":\"" + userName + "\", \"password\":\"" + password + "\"}";
            // convert JSON to raw bytes
            byte[] rawBody = Encoding.UTF8.GetBytes(bodystring);
            serverConnection.uploadHandler = new UploadHandlerRaw(rawBody);
            serverConnection.downloadHandler = new DownloadHandlerBuffer();
            serverConnection.SetRequestHeader("Content-Type", "application/json");

            yield return serverConnection.SendWebRequest();

            switch (serverConnection.result)
            {
                case UnityWebRequest.Result.InProgress:
                    break;
                case UnityWebRequest.Result.Success:
                    string jsonData = serverConnection.downloadHandler.text;
                    PlayerData UserData = JsonConvert.DeserializeObject<PlayerData>(jsonData);
                    UpdateUserValues(UserData);
                    break;
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(serverConnection.error);
                    break;
                default:
                    break;
            }
        }
    }
    
    IEnumerator GetRankingCoroutine()
    {
        string path = _baseURL + "/ranking";
        using (UnityWebRequest serverConnection = new UnityWebRequest(path, "GET"))
        {
            serverConnection.downloadHandler = new DownloadHandlerBuffer();
        
            yield return serverConnection.SendWebRequest();
           
            switch (serverConnection.result)
            {
                case UnityWebRequest.Result.InProgress:

                    break;
                case UnityWebRequest.Result.Success:
                    string jsonData = serverConnection.downloadHandler.text;
                    PlayerData[] Ranking = JsonConvert.DeserializeObject<PlayerData[]>(jsonData);
                    Debug.Log(jsonData);
                    UpdateRankingsData(Ranking);
                    OnRankingRecieved?.Invoke();
                    break;
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.ProtocolError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log(serverConnection.error);
                    break;
                default:
                    break;
            }
        }
    }
}
