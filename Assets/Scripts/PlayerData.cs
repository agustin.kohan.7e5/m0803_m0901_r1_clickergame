using System;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public int Id;
    public string UserName;
    public string Password;
    public int Score;

    public PlayerData()
    {
        UserName = "null Name";
        Password = "null Pasword";
        Score = 0;
    }
    public PlayerData(int id, string userName, string password, int? score)
    {
        Id = id;
        UserName = userName;
        Password = password;
        Score = score ?? 0;
    }
}
